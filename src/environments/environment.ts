// export const environment = {
//   production: false,
//   API_URL: 'http://localhost:8080',
//   SPRING_API_URL: 'http://localhost:8080',
//   VERSION: '2.12.1'
// };

export const environment = {
  production: true,
  API_URL: 'https://twvph9vsz9.execute-api.us-east-1.amazonaws.com/production/',
  VERSION: '0.0.0'
};

// export const environment = {
//   production: false,
//   API_URL: 'http://api.homologacao.agendafacilplus.com.br/homolog',
//   SPRING_API_URL: 'http://api.homologacao.agendafacilplus.com.br/homolog',
//   VERSION: '2.12.1'
// };