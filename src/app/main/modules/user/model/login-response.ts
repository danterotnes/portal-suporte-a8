import { User } from './user';
import { UserManagement } from './user-management';

export interface LoginResponse {
  authentification: string;
  user: User;
  status: UserManagement;
}
