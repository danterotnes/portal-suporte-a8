import { UserType } from './user-type';

export interface UserManagement {
  type: UserType;
}
