import { Pipe, PipeTransform } from '@angular/core';
import { UserType } from './model';

@Pipe({
  name: 'userType'
})
export class UserTypePipe implements PipeTransform {
  transform(value: UserType, args?: any): string {
    switch (value) {
      case UserType.ADM:
        return 'ADM';
      case UserType.VIEWER:
        return 'VIEWER';
      default:
        return 'Não identificado';
    }
  }
}
