import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromUsers from './user.reducer';

export const selectUsersState = createFeatureSelector<fromUsers.UserState>('user');

export const selectAllUsers = createSelector(
  selectUsersState,
  fromUsers.selectAll
);

export const selectLoggedInUser = createSelector(
  selectAllUsers,
  (users) => {
    if (users && users.length > 0) {
      return users[0];
    }
    return undefined;
  }
);

export const selectLoggedInUserEmail = createSelector(
  selectLoggedInUser,
  (user) => {
    if (user) {
      return user.email;
    }
    return null;
  }
);

export const selectIsUserLoading = createSelector(
  selectUsersState,
  (state) => state.loading
);

export const selectAuthToken = createSelector(
  selectUsersState,
  (state) => (state ? state.authentification : '')
);

export const selectStatusType = createSelector(
  selectUsersState,
  (state) => (state ? (state.status ? state.status.type : null) : null)
);

