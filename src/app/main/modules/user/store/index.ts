import { AuthHeaderInterceptor } from './auth-header.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthErrorInterceptor } from './auth-error.interceptor';

export * from './auth-header.interceptor';
export * from './auth-error.interceptor';
export * from './side-effects';
export * from './user.actions';
export * from './user.effects';
export * from './user.reducer';
export * from './user.selectors';

export const AuthProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthHeaderInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: AuthErrorInterceptor, multi: true }
];
