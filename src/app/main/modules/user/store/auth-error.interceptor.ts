import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Router } from '@angular/router';
import { LocalStorageService } from './side-effects';
import { AppState } from 'src/app/reducers';
import { Store } from '@ngrx/store';
import { LogOffUserStart } from './user.actions';

@Injectable()
export class AuthErrorInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private localStorageService: LocalStorageService,
    private store: Store<AppState>
  ) {}
  private isSessionExpiredError = (error: HttpErrorResponse): boolean => {
    if (error.error == 'User not logged or session expired' && error.status == 400) {
      return true;
    }
    return false;
  };
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        if (this.isSessionExpiredError(error)) {
          this.localStorageService.clear();
          this.store.dispatch(new LogOffUserStart());
          // this.notification.notify(
          //   NotificationOrigin.TOP,
          //   NotificationAlignment.RIGHT,
          //   NotificationType.DANGER,
          //   'Sua sessão expirou.Favor efetuar login novamente.'
          // );
          this.router.navigate(['/login'], {
            queryParams: { redirect: this.router.routerState.snapshot.url }
          });
          return [];
        }

        return throwError(error);
      })
    );
  }
}
