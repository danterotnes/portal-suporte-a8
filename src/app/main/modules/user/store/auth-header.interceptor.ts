import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginResponse } from '../model/login-response';

@Injectable()
export class AuthHeaderInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with basic auth credentials if available
    let currentUser: LoginResponse = JSON.parse(localStorage.getItem('user_data'));
    
    if (currentUser && currentUser.authentification) {
      request = request.clone({
        setHeaders: {
          Authorization: `${currentUser.authentification}`
        }
      });
    }

    return next.handle(request);
  }
}
