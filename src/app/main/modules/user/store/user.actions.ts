import { Action } from '@ngrx/store';
import { LoginInfo } from '../model/login-info';
import { User } from '../model/user';
import { LoginResponse } from '../model/login-response';

export enum UserActionTypes {
  LoginUserStart = '[User] Login User Start',
  LoginUserSuccess = '[User] Login User Success',
  LoginUserError = '[User] Login User Error',
  LogOffUserStart = '[User] Log off User Start',
  LogOffUserSuccess = '[User] Log off User Success',
  LogOffUserError = '[User] Log off User Error'
}

export class LoginUserStart implements Action {
  readonly type = UserActionTypes.LoginUserStart;
  constructor(public payload: { loginInfo: LoginInfo}) {}
}

export class LoginUserSuccess implements Action {
  readonly type = UserActionTypes.LoginUserSuccess;
  constructor(public payload: { loginResponse: LoginResponse }) {}
}

export class LoginUserError implements Action {
  readonly type = UserActionTypes.LoginUserError;
}

export class LogOffUserStart implements Action {
  readonly type = UserActionTypes.LogOffUserStart;
}

export class LogOffUserSuccess implements Action {
  readonly type = UserActionTypes.LogOffUserSuccess;
}

export class LogOffUserError implements Action {
  readonly type = UserActionTypes.LogOffUserError;
}

export type UserActions =
  | LoginUserStart
  | LoginUserSuccess
  | LoginUserError
  | LogOffUserStart
  | LogOffUserSuccess
  | LogOffUserError;
