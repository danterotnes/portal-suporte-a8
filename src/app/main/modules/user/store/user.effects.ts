import { LocalStorageService } from './side-effects/local-storage.service';
import { UserService } from './side-effects/user.service';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {
  UserActionTypes,
  LoginUserStart,
  LoginUserSuccess,
  LogOffUserStart,
  LogOffUserSuccess,
  LoginUserError
} from './user.actions';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/reducers';
import { LoginResponse } from '../model/login-response';

@Injectable()
export class UserEffects {
  @Effect()
  logInUser$ = this.actions$.pipe(
    ofType<LoginUserStart>(UserActionTypes.LoginUserStart),
    mergeMap((action) =>
      this.userService.logInUser(action.payload.loginInfo)
    ),
    catchError((error) => {
      
      this.store.dispatch(new LoginUserError());
      return [];
    }),
    map((response: { loginResponse: LoginResponse; }) => {
      if(response.loginResponse.authentification != 'error'){
        this.localStorageService.set(response.loginResponse);
        if (response) {
          this.router.navigate(['/home']);
        } else {
          this.router.navigate(['/']);
        }
        return new LoginUserSuccess({ loginResponse: response.loginResponse });
      }
      return new LoginUserError();

    })
  );

  @Effect()
  logOffUser$ = this.actions$.pipe(
    ofType<LogOffUserStart>(UserActionTypes.LogOffUserStart),
    map((user) => {
      this.localStorageService.clear();
      this.router.navigate(['/login']);
      return new LogOffUserSuccess();
    })
  );

  constructor(
    private actions$: Actions,
    private userService: UserService,
    private localStorageService: LocalStorageService,
    private router: Router,
    private store: Store<AppState>
  ) {}
}
