import { Injectable } from '@angular/core';
import { LoginInfo } from '../../model/login-info';
import { User } from '../../model/user';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { LoginResponse } from '../../model/login-response';
import swal from 'sweetalert2'

@Injectable()
export class UserService {
  API_URL: string = environment.API_URL;

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json'
    })
  };
  constructor(private http: HttpClient) {}

  logInUser(userInfo: LoginInfo) {
    return this.http.post<LoginResponse>(`${this.API_URL}/ferramentaSupLogin-cf`, {userInfo}, this.httpOptions).pipe(
      catchError((error) => {
        if (error.status === 401) {
          swal.fire({
            title: 'Error!',
            text: 'Ocorreu um erro na sua solicitação',
            icon: 'error',
            confirmButtonText: 'Tentar Novamente'
          })
          throw error;
        }
        swal.fire({
          title: 'Error!',
          text: 'Ocorreu um erro na sua solicitação',
          icon: 'error',
          confirmButtonText: 'Tentar Novamente'
        })
        throw error;
      }),
      map((loginResponse: LoginResponse) => {
        if(loginResponse.authentification == 'error'){
          swal.fire({
            title: 'Error!',
            text: 'Email ou Senha incorretos',
            icon: 'error',
            confirmButtonText: 'Tentar Novamente'
          })
        }
        console.log(loginResponse);
        return { loginResponse };
      })
    );
  }
}
