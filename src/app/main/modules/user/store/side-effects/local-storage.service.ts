import { Injectable } from '@angular/core';
import { LoginResponse } from '../../model/login-response';
import * as moment from 'moment';

@Injectable()
export class LocalStorageService {
  constructor() {}

  get(): LoginResponse {
    if (moment(new Date(localStorage.getItem('user_data'))).isAfter(moment())) {
      this.clear();
      return null;
    }
    return JSON.parse(localStorage.getItem('user_data')) as LoginResponse;
  }

  getExpiresAt(): Date {
    return new Date(localStorage.getItem('expiresAt'));
  }

  set(user: LoginResponse) {
    const userData = JSON.stringify(user);
    const date = moment(new Date())
      .add(2, 'hours')
      .toISOString();
    localStorage.setItem('user_data', userData);
    localStorage.setItem('expiresAt', date);
  }

  clear() {
    localStorage.removeItem('user_data');
    localStorage.removeItem('expiresAt');
  }
}
