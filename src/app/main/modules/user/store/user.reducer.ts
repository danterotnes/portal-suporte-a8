import { UserActions, UserActionTypes } from './user.actions';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { User } from '../model/user';
import { UserManagement } from '../model/user-management';

export interface UserState extends EntityState<User> {
  loading: boolean;
  authentification: string;
  status: UserManagement;
}

export const adapter: EntityAdapter<User> = createEntityAdapter<User>();

export const initialState: UserState = adapter.getInitialState({
  loading: false,
  authentification: '',
  status: { type: null }
});

export function UserReducer(state = initialState, action: UserActions): UserState {
  switch (action.type) {
    case UserActionTypes.LoginUserStart || UserActionTypes.LogOffUserStart:
      return { ...state, loading: true };

    case UserActionTypes.LoginUserSuccess:
      const { authentification, user, status } = action.payload.loginResponse;
      return adapter.addOne(user, {
        ...adapter.removeAll(state),
        loading: false,
        authentification,
        status
      });

    case UserActionTypes.LoginUserError:
      return { ...state, loading: false };

    case UserActionTypes.LogOffUserSuccess:
      return adapter.removeAll({ ...state, loading: false });

    default:
      return state;
  }
}

export const { selectAll, selectEntities, selectIds, selectTotal } = adapter.getSelectors();
