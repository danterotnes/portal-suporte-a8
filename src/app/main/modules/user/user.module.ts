import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { UserReducer, UserEffects, UserService, LocalStorageService } from './store';
import { LoginComponent } from '../../login';
import { RouterModule } from '@angular/router';
import { UserTypePipe } from './user-type.pipe';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

const declarations = [UserTypePipe];

@NgModule({
  declarations: [...declarations],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    StoreModule.forFeature('user', UserReducer),
    EffectsModule.forFeature([UserEffects])
  ],
  exports: [...declarations],
  providers: [LocalStorageService, UserService],

  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class UserModule {}
