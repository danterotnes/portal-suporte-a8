import { AppState } from 'src/app/reducers';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { LocalStorageService } from './store/side-effects/local-storage.service';
import { LoginUserSuccess } from './store/user.actions';
import { selectUsersState } from './store/user.selectors';
import { map } from 'rxjs/operators';
import { LoginResponse } from './model/login-response';
import { UserState, selectAll } from './store/user.reducer';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private store: Store<AppState>,
    private router: Router,
    private localStorageService: LocalStorageService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.store.pipe(
      select(selectUsersState),
      map((user: UserState) => {
        if (user) {
          if (selectAll(user).length === 0) {
            const loginResponse: LoginResponse = this.localStorageService.get();
            if (!loginResponse) {
              this.router.navigate(['/login'], {
                queryParams: { redirect: state.url }
              });
              return false;
            }
            this.store.dispatch(new LoginUserSuccess({ loginResponse }));
            return true;
          }
          return true;
        }
        return false;
      })
    );
  }
}
