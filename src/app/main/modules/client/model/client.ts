export interface Client {
  idCliente: number;
  nome: string;
  cnpj: string;
  schema:string;
  razaoSocial: string;
  status: string;
  franquia: number;
  dataFaturamento: string;
  notasFaturamento: number;
  
}
