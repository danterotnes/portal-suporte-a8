import { Observable } from 'rxjs';
import { LoginUserStart } from '../modules/user/store/user.actions';
import { AppState } from '../../reducers/index';
import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { selectIsUserLoading } from '../modules/user/store/user.selectors';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  $loading: Observable<boolean>;
  redirect: string;
  form: FormGroup;
  loginFormControl: FormControl;
  passwordFormControl: FormControl;

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.loginFormControl = new FormControl(null, [
      Validators.required,
      Validators.email
    ]);
    this.passwordFormControl = new FormControl(null, [Validators.required]);
  }

  ngOnInit() {
    this.setInitialVisualState();
    this.activatedRoute.queryParams.subscribe((params) => {
      this.redirect = params.redirect;
    });

    this.$loading = this.store.pipe(select(selectIsUserLoading));

    this.form = this.fb.group({
      login: this.loginFormControl,
      password: this.passwordFormControl
    });
  }

  handleLogInClick() {
    if (this.form.valid) {
      const { login, password } = this.form.value;
      this.store.dispatch(
        new LoginUserStart({
          loginInfo: {
            login,
            password
          }
        })
      );
    }
  }

  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('login-page');
    body.classList.remove('off-canvas-sidebar');
  }

  setInitialVisualState() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('login-page');
    body.classList.add('off-canvas-sidebar');
  }
}
